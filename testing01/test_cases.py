import funcs

class TestsClass:

    def test_should_succeed(self):
        assert funcs.func(3) == 4

    def test_should__not_fail_now(self):
        assert funcs.func(4) == 5
